# AUI可视化布局工具
### OkayDesigner是可拖拽的在线或离线前端布局工具，它整合了AUI CSS框架、Font Awesome图标库。OkayDesigner既可以像墨刀一样供产品经理快速构建原型，同时提供了生成源代码给前端人员，快速生成产品，让开发人员可以更专注与业务逻辑，减少在页面上了花费的功夫。OkayDesigner还提供了在线脚本编辑，可以让Javascript脚本直接应用于预览页面，使产品设计预览阶段就可以清晰展现交互功能，更好理解需求。

### OkayDesigner除了整合AUI CSS框架，开发人员还可以根据自己的意愿，按照OkayDesigner的开发要求整合其他CSS框架或自定义框架。

### OkayerDesigner预览页面http://okaydesigner.djxfire.cn
